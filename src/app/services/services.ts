import { Injectable } from '@angular/core';
import { Http, HttpModule, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Hotel } from './hotel.model';

@Injectable()
export class HttpService {
  private url : string;
  
  private _stream_name$ = new BehaviorSubject('');
  public stream_name$ = this._stream_name$.asObservable();
  
  private _stream_stars$ = new BehaviorSubject('');
  public stream_stars$ = this._stream_stars$.asObservable();

  constructor (private http: Http) {
    this.url = 'http://localhost:3300';
  }

  submitHotelName(name: string) {
    this._stream_name$.next(name);
  }

  submitHotelStars(starsSelection) {
    this._stream_stars$.next(starsSelection);
  }

  getHotels(): Observable<Hotel[]> {
    return this.http.get(this.url + '/')
      .map((response: Response) => response.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getHotelsByStars (starsSelection): Observable<Hotel[]> {
    return this.http.get(this.url + '/stars/' + starsSelection)
      .map((response: Response) => response.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getHotelsByName(name: string): Observable<Hotel[]> {
    let nameT = name;
  	return this.http.get(this.url + '/hotels/' + nameT)
  		.map((response: Response) => response.json())
    	.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}