import { Component } from '@angular/core';
import { HttpService } from '../services/services';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})

export class FiltersComponent {
  options = [
    {
      'name': '5',
      'value': 5,
      'checked': false
    },
    {
      'name': '4',
      'value': 4,
      'checked': false
    },
    {
      'name': '3',
      'value': 3,
      'checked': false
    },
    {
      'name': '2',
      'value': 2,
      'checked': false
    },
    {
      'name': '1',
      'value': 1,
      'checked': false
    }
  ];
  starsSelection = [];
  toggleFilter = {
    'search': true,
    'stars': true,
    'wrapper': false
  };

  constructor (private httpService: HttpService) {
  }

  searchHotel (name: string) {
    //change
    this.httpService.submitHotelName(name);
  }

  searchStars (stars: number, checked: boolean) {
    if (checked)
      this.starsSelection.push(stars);
    else
      this.starsSelection.splice(this.starsSelection.indexOf(stars), 1);

    this.httpService.submitHotelStars(this.starsSelection);
  }

  starsIteration (stars: number) {
    return new Array(stars);
  }

}