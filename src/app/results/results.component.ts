import { Component } from '@angular/core';
import { HttpService } from '../services/services';
import { Hotel } from '../services/hotel.model';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})

export class ResultsComponent {
  hotels: Hotel[];
  name: string;
  Math: any;

  constructor (private httpService: HttpService) { 
    this.httpService.stream_name$.subscribe(this.getHotelByName.bind(this));
    this.httpService.stream_stars$.subscribe(this.getHotelsByStars.bind(this));
    this.Math = Math;
  }

  ngOnInit(): void {
    this.getHotels();
  }

  getHotels(): void {
    this.httpService.getHotels().subscribe(hotels => {this.hotels = hotels;})
  }

  getHotelByName ( name: string ) {
    this.httpService.getHotelsByName(name).subscribe(hotels => {this.hotels = hotels;})
  }

  getHotelsByStars ( optionsQuery ) {
    this.httpService.getHotelsByStars(optionsQuery).subscribe(hotels => {this.hotels = hotels;})
  }

  starsIteration (stars: number) {
    return new Array(stars);
  }

}