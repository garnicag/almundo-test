# Almundo
This is the README.md of the Front End test for almundo by Gabriel Garnica.

## Setup

Before start you'll need: `git`, `node` version 8+, `npm` and `yarn`.

1. Open a terminal window
2. Clone this repo `git clone https://bitbucket.org/garnicag/almundo-test.git`

### Back End Setup

1. Open a new terminal window
2. Go to almundo/api folder `cd almundo-test/api`
3. Install dependencies `npm i`

### Front End Setup

1. Open a new terminal window
2. Go to almundo folder `cd almundo-test`
3. Install dependencies `yarn`

## Development server

First run the API server:

1. Open a new terminal window
2. Go to almundo folder `cd almundo-test/api`
3. Run the command `npm start`

Then run `ng serve` for a dev server.
Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
