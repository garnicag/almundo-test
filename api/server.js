var app = require('./api');
var port = process.env.PORT || 3300;

var server = app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});