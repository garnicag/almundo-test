var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));

var Hotel = require('./hotelModel');

//CREATE
router.post('/create', function (req, res) {
	Hotel.create({
		id : req.body.id,
		name : req.body.name,
		stars : req.body.stars,
		price : req.body.price,
		image : req.body.image,
		amenities : req.body.amenities
	}, 
	function (err, hotel) {
		if (err) return res.status(500).send("Error creating new hotel.");
		res.status(200).send(hotel);
	});
});

//READ
router.get('/read/:id', function (req, res) {
	Hotel.findById(req.params.id, function (err, hotel) {
		if (err) return res.status(500).send("Error finding hotel.");
		res.status(200).send(hotel);
	});
});

//UPDATE
router.put('/update/:id', function (req, res) {
	Hotel.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, hotel) {
		if (err) return res.status(500).send("Error updating hotel.");
		res.status(200).send(hotel);
	});
});

//DELETE
router.delete('/delete/:id', function (req, res) {
	Hotel.findByIdAndRemove(req.params.id, function (err, hotel) {
		if (err) return res.status(500).send("Error finding hotel.");
		res.status(200).send("Item " + hotel.name + " deleted.");
	});
});

//Get Complete List
router.get('/', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET');
	Hotel.find({}, function (err, hotels) {
		if (err) return res.status(500).send("Error finding hotel.");
		res.status(200).send(hotels);
	});
});

//Filter by name
router.get('/hotels/:name', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET');
	Hotel.find({'name': new RegExp(req.params.name, 'i')}, function (err, hotel) {
		if (err) return res.status(500).send("Error finding hotel.");
		if (!hotel) return res.status(404).send("No hotels found.");
		res.status(200).send(hotel);
	});
});

//Filter by stars
router.get('/stars/:stars', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET');
	console.log(req.params.stars);
	Hotel.find({'stars' : req.params.stars}, function (err, hotel) {
		if (err) return res.status(500).send("Error finding hotel.");
		if (!hotel) return res.status(404).send("No hotels found.");
		res.status(200).send(hotel);
	});
});

router.get('/hotels/', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET');
	Hotel.find({}, function (err, hotels) {
		if (err) return res.status(500).send("Error finding hotel.");
		res.status(200).send(hotels);
	});
});

router.get('/stars/', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET');
	Hotel.find({}, function (err, hotels) {
		if (err) return res.status(500).send("Error finding hotel.");
		res.status(200).send(hotels);
	});
});

module.exports = router;